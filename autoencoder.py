#!/usr/bin/env python
# coding: utf-8

# In[ ]:


from keras.layers import Input, Dense
from keras.models import Model
def autoencoder_dimensionaltyReduction(inputshape):
    # this is our input placeholder
    input_img = Input(shape=(inputshape,))
    # "encoded" is the encoded representation of the input
    encoded = Dense(128, activation='relu')(input_img)
    encoded = Dense(64, activation='relu')(encoded)
    encoded = Dense(32, activation='relu')(encoded)
    encoded = Dense(10, activation='relu')(encoded)
    encoded2 = Dense(5, activation='relu')(encoded)

    decoded = Dense(10, activation='relu')(encoded2)
    decoded = Dense(32, activation='relu')(decoded)
    decoded = Dense(64, activation='relu')(decoded)
    decoded = Dense(128, activation='relu')(decoded)
    # "decoded" is the lossy reconstruction of the input
    decoded = Dense(inputshape, activation='sigmoid')(encoded)

    # this model maps an input to its reconstruction
    autoencoder = Model(input_img, decoded)
    encoder = Model(input_img, encoded2)
    return autoencoder,encoder


# In[ ]:




