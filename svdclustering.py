#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import rasterio
from rasterio import mask
import numpy as np
import matplotlib.pyplot as plt
import geopandas as gpd
import os
from scipy.linalg import svd
from sklearn.decomposition import TruncatedSVD
import numpy as np
from sklearn.cluster import KMeans
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.patches as mpatches


# loading filenames from folder
def load_imagesfilenames_from_folder(folder):
    file_list = []
    for filename in os.listdir(folder):
        _, file_extension = os.path.splitext(filename)
        if(file_extension=='.TIF'):
            filenamet=os.path.join(folder,filename)
            file_list.append(filenamet)
    return file_list

def stackingfiles(path_to_folder,stacking_filename):
    
    
    #parameters
    #path_to_folder= folder path where the hyperion bands are present
    #stacking_filename=Output file name to store stacked file
    
    # return: stacked object,stacked array
    file_list=load_imagesfilenames_from_folder(path_to_folder)         
    stacked_path = stacking_filename #Path of new stacked file

    # Read metadata of first file
    with rasterio.open(file_list[0]) as src0:
        meta = src0.meta

    # Update meta to reflect the number of layers
    meta.update(count = len(file_list))

    # Read each layer and write it to stack
    with rasterio.open(stacked_path, 'w', **meta) as dst:
        for id, layer in enumerate(file_list, start=1):
            with rasterio.open(layer) as src1:
                dst.write_band(id, src1.read(1))
    
    raster = rasterio.open(stacked_path)
    raster_arr = raster.read()
    return raster,raster_arr
def clippingRasterfile(input_raster,output_clipped_filename,polygonarray):
    #parameters
    #path_to_folder= inputfilename
    #output_clipped_filename=Output file name to store clipped file
    
    # return: clipped_array
    #clip the stacked file
    with rasterio.open(input_raster) as src:
        out_image, out_transform = rasterio.mask.mask(src,polygonarray, crop=True)
        out_meta = src.meta
        out_meta.update({"driver": "GTiff",
                 "height": out_image.shape[1],
                  "width": out_image.shape[2],
                 "transform": out_transform,}
                                     )
    with rasterio.open(output_clipped_filename, "w", **out_meta) as dest:
        dest.write(out_image)
    return out_image



def SVDTruncated_kmeans(input_array,components_count,clusterCount):
    ##preprocessing the data

    # conversion of format (bands,rows,columns) to (rows,columns,bands)
    row, col, bands = int(input_array.shape[1]), int(input_array.shape[2]),int(input_array.shape[0])
    input_data=np.ndarray(shape=(row,col,bands))
    for j in range(bands):
        input_data[:,:,j]=input_array[j,:,:]
    #reshape row* columns *bands to 1d*bands
    reshape_inputdata=input_data.reshape(input_data.shape[0]*input_data.shape[1],input_data.shape[2])
    svd = TruncatedSVD(n_components=10)
    svd.fit(reshape_inputdata)
    svd_output = svd.transform(reshape_inputdata)
    kmeans = KMeans(n_clusters=clusterCount, random_state=0).fit(svd_output)
    labels=kmeans.labels_
    labeloutput_2d=labels.reshape(input_array.shape[1],input_array.shape[2],)
    svd_2d=svd_output.reshape(input_array.shape[1],input_array.shape[2],10)
    return svd_2d,labeloutput_2d

def SVDTruncated_level2_kmeans(input_array,components_count,cluster,labels,clusterCount):
    ##preprocessing the data
    labelsarray=labels.reshape(input_array.shape[1]*input_array.shape[2],)
    labeladdresses=np.argwhere(labelsarray==cluster)
    
    # conversion of format (bands,rows,columns) to (rows,columns,bands)
    row, col, bands = int(input_array.shape[1]), int(input_array.shape[2]),int(input_array.shape[0])
    input_data=np.ndarray(shape=(row,col,bands))
    for j in range(bands):
        input_data[:,:,j]=input_array[j,:,:]
    #reshape row* columns *bands to 1d*bands
    reshape_inputdata=input_data.reshape(input_data.shape[0]*input_data.shape[1],input_data.shape[2])
    pixel_subset=[]
    for i in labeladdresses:
        pixel_subset.append(reshape_inputdata[i[0]])
    svd = TruncatedSVD(n_components=10)
    svd.fit(pixel_subset)
    svd_output = svd.transform(pixel_subset)
    kmeans = KMeans(n_clusters=clusterCount, random_state=0).fit(svd_output)
    labels_2=kmeans.labels_
    secondlevel_labels=np.zeros((input_array.shape[1]*input_array.shape[2]))
    j=0;
    for i in labeladdresses:
        secondlevel_labels[i[0]]=labels_2[j]+1
        j=j+1;
    labeloutput_secondlevel=secondlevel_labels.reshape(int(input_array.shape[1]), int(input_array.shape[2]))
    return labeloutput_secondlevel
def CMAP(class_image):
    n = int(class_image.max()+1)
    colors = dict((
         # Nodata
        (0, [128,128,128]), #
        (1, [0,128,0]),  # 
        (2, [0,119,190]),  # 
        (3, [255, 0, 0]),  # 
        (4, [255,255,0]),  #
    ))
    for k in colors:
        v = colors[k]
        _v = [_v / 255.0 for _v in v]
        colors[k] = _v
#     print(colors)
    index_colors = [colors[key] if key in colors else 
                (255, 255, 255, 0) for key in range(0, n+1)]
    cmap = plt.matplotlib.colors.ListedColormap(index_colors, 'Classification', n)
    return cmap
def visualize_image(class_image, new_input_fcc,label1,label2):
    n = int(class_image.max()+1)
    colors = dict((
         # Nodata
        (0, [128,128,128]), #
        (1, [0,128,0]),  # 
        (2, [0,119,190]),  # 
        (3, [255, 0, 0]),  # 
        (4, [255,255,0]),  #
    ))
    for k in colors:
        v = colors[k]
        _v = [_v / 255.0 for _v in v]
        colors[k] = _v
#     print(colors)
    index_colors = [colors[key] if key in colors else 
                (255, 255, 255, 0) for key in range(0, n+1)]
    cmap = plt.matplotlib.colors.ListedColormap(index_colors, 'Classification', n)
    get_ipython().run_line_magic('matplotlib', 'inline')
    fig = plt.figure(figsize = (15,15))
    plt.subplot(121)
    ax = fig.gca()
    normalized = new_input_fcc
    im = ax.imshow(normalized)
    plt.title(label1)

    divider = make_axes_locatable(ax)

    plt.subplot(122)
    ax = fig.gca()
    im = ax.imshow(class_image, cmap=cmap, interpolation='none')
    # plt.colorbar(shrink=0.5)
    plt.title(label2)
    
    gray_patch = mpatches.Patch(color='Gray', label='Cluster1')
    green_patch = mpatches.Patch(color='Green', label='Cluster2')
    blue_patch = mpatches.Patch(color='Darkblue', label='Cluster3')
    red_patch = mpatches.Patch(color='Red', label='Cluster4')
    yellow_patch = mpatches.Patch(color='Yellow', label='Cluster5')
    
    colorlist=[gray_patch,green_patch,blue_patch,red_patch,yellow_patch]
    plt.legend(loc='upper right',handles=colorlist[0:n],bbox_to_anchor=(1.4, 1))
    divider = make_axes_locatable(ax)
    plt.show()

